<?php
/*
 * Copyright (c) 2020, Juan (juan.valero@etu.univ-lyon1.fr), All rights reserved
 */

use Annotations\Exceptions\AnnotationException;
use Example\ExampleController;
use HTTP\Request\Server\ServerRequestFactory;
use Router\Exceptions\NoRouteMatchedException;
use Router\Route;
use Router\Router;

require '../../vendor/autoload.php';
require '../functions.php';

try {
    $serverRequest = ServerRequestFactory::createServerRequestFromGlobals();

    $router = new Router();

    $router->addRoute(
        new Route(
            'index',
            'index',
            function () {
                echo 'You are currently on the index page';
            }
        )
    );

    $router->addRoute(new ExampleController());

    $router->match($serverRequest);
} catch (AnnotationException|NoRouteMatchedException $e) {
    echo $e->getMessage();
} catch (ReflectionException $ignored) {
}