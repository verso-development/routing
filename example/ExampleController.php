<?php
/*
 * Copyright (c) 2020, Juan (juan.valero@etu.univ-lyon1.fr), All rights reserved
 */

namespace Example;

use Router\Annotations\Route;

class ExampleController
{
    /**
     * @Route(name="blog", path="blog/{id}-{slug}", requirements={"id": "\\d+"})
     *
     * @param int $id
     */
    public function blog(int $id)
    {
        printf('Welcome on the article n°%d', $id);
    }
}