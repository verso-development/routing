<?php
/*
 * Copyright (c) 2020, Juan (juan.valero@etu.univ-lyon1.fr), All rights reserved
 */

namespace Router\Exceptions;

use Exception;

class NoRouteMatchedException extends Exception
{
    /**
     * NoRouteMatchedException constructor.
     */
    public function __construct()
    {
        parent::__construct('No route matched');
    }
}