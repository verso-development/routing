<?php
/*
 * Copyright (c) 2020, Juan (juan.valero@etu.univ-lyon1.fr), All rights reserved
 */

namespace Router;

class RouteResult
{
    private bool $matched;
    private array $parameters;
    /**
     * @var callable
     */
    private $callable;

    /**
     * RouteResult constructor.
     * @param bool $matched
     * @param array $parameters
     * @param callable $callable
     */
    public function __construct(bool $matched, array $parameters, callable $callable)
    {
        $this->matched = $matched;
        $this->parameters = $parameters;
        $this->callable = $callable;
    }

    /**
     * @return bool
     */
    public function hasMatched(): bool
    {
        return $this->matched;
    }

    /**
     * @return callable
     */
    public function getCallable(): callable
    {
        return $this->callable;
    }

    /**
     * @return array
     */
    public function getParameters(): array
    {
        return $this->parameters;
    }
}