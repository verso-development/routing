<?php
/*
 * Copyright (c) 2020, Juan (juan.valero@etu.univ-lyon1.fr), All rights reserved
 */

namespace Router\Annotations;

use Annotations\Annotations\Annotation;
use Annotations\Annotations\Target;

/**
 * Class Route
 * @package Router\Annotations
 *
 * @Annotation
 * @Target(["METHOD"])
 */
class Route
{
    public string $name;
    public string $path;
    public array $requirements = [];
}