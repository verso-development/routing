<?php
/*
 * Copyright (c) 2020, Juan (juan.valero@etu.univ-lyon1.fr), All rights reserved
 */

namespace Router;

class Route
{
    private string $name;
    private string $path;
    /**
     * @var callable
     */
    private $callable;
    private array $requirements;

    /**
     * Route constructor.
     * @param string $name
     * @param string $path
     * @param callable $callable
     * @param array $requirements
     */
    public function __construct(string $name, string $path, callable $callable, array $requirements = [])
    {
        $this->name = $name;
        $this->path = $path;
        $this->callable = $callable;
        $this->requirements = $requirements;
    }

    public function match(string $path): RouteResult
    {
        preg_match_all('/{([a-zA-Z]+)}/', $this->path, $params);

        foreach ($params[1] as $param) {
            $rule = array_key_exists($param, $this->requirements)
                ? $this->requirements[$param]
                : '.*'; // Match any character

            $this->path = str_replace(sprintf('{%s}', $param), sprintf('(%s)', $rule), $this->path);
        }

        $regex = '/' . addcslashes($this->path, '/') . '/';

        $matched = preg_match($regex, $path, $matches);
        unset($matches[0]);

        $parameters = [];
        foreach ($matches as $key => $match) {
            $parameters[$params[1][$key - 1]] = $match;
        }

        return new RouteResult($matched, $parameters, $this->callable);
    }
}