<?php
/*
 * Copyright (c) 2020, Juan (juan.valero@etu.univ-lyon1.fr), All rights reserved
 */

namespace Router;

use Annotations\AnnotationReader;
use Annotations\Exceptions\AnnotationException;
use Psr\Http\Message\ServerRequestInterface;
use ReflectionClass;
use ReflectionException;
use Router\Annotations\Route as RouteAnnotation;
use Router\Exceptions\NoRouteMatchedException;

class Router
{
    /**
     * @var Route[]
     */
    private array $routes;

    /**
     * @param object|Route $route
     * @throws AnnotationException
     * @throws ReflectionException
     */
    public function addRoute($route): void
    {
        if (!($route instanceof Route)) {
            $annotationReader = new AnnotationReader();
            $class = new ReflectionClass($route);

            foreach ($class->getMethods() as $method) {
                $annotations = $annotationReader->getMethodAnnotations($method);
                $annotationRoute = $annotations[RouteAnnotation::class];

                $route = new Route(
                    $annotationRoute->name,
                    $annotationRoute->path,
                    function (array $args) use ($method, $route) {
                        $method->invokeArgs($route, $args);
                    },
                    $annotationRoute->requirements
                );
            }
        }

        $this->routes[] = $route;
    }

    /**
     * @param ServerRequestInterface $serverRequest
     * @throws NoRouteMatchedException
     */
    public function match(ServerRequestInterface $serverRequest): void
    {
        $path = $serverRequest->getUri()->getPath();

        foreach ($this->routes as $route) {
            $result = $route->match($path);

            if ($result->hasMatched()) {
                call_user_func($result->getCallable(), $result->getParameters());
                return;
            }
        }

        throw new NoRouteMatchedException();
    }
}